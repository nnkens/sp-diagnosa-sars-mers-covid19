kondisi = [
    ["0", "Tidak", 0],
    ["1", "Ragu-ragu", 0.2],
    ["2", "Mungkin", 0.4],
    ["3", "Kemungkinan Besar", 0.6],
    ["4", "Hampir Pasti", 0.8],
    ["5", "Pasti", 1],
]
penyakit = ["SARS", "MERS", "COVID-19"]
gejala = ["Demam", "Batuk", "Kelelahan", "Sesak Nafas", "Rasa tidak enak", "Sakit dan nyeri tubuh", "Sakit kepala", "Menggigil", "Nyeri otot",
          "Ingus atau hidung tersumbat", "Sakit tenggorokan", "Mual", "Diare", "Hilang rasa", "Hilang bau", "Batuk tidak berdahak", "Batuk berdarah"]

# rulecf[0]: Penyakit
# rulecf[1]: Gejala
# rulecf[2]: CF Pakar (Data Skala nilai bobot yang diberikan oleh Pakar)
# rulecf[3]: CF User  (Gejala yang dirasakan Pasien)
rulecf = [
    ["SARS", "Demam", 0.8, 0],
    ["SARS", "Batuk", 0.8, 0],
    ["SARS", "Rasa tidak enak", 0.8, 0],
    ["SARS", "Sakit dan nyeri tubuh", 0.8, 0],
    ["SARS", "Sakit kepala", 0.8, 0],
    ["SARS", "Sesak Nafas", 0.8, 0],
    ["SARS", "Menggigil", 0.8, 0],
    ["SARS", "Diare", 0.6, 0],
    ["SARS", "Batuk tidak berdahak", 0.4, 0],
    ["MERS", "Demam", 0.8, 0],
    ["MERS", "Batuk", 0.8, 0],
    ["MERS", "Menggigil", 0.8, 0],
    ["MERS", "Nyeri otot", 0.8, 0],
    ["MERS", "Sesak Nafas", 0.6, 0],
    ["MERS", "Batuk berdarah", 0.4, 0],
    ["MERS", "Mual", 0.6, 0],
    ["MERS", "Diare", 0.6, 0],
    ["COVID-19", "Demam", 0.8, 0],
    ["COVID-19", "Batuk", 0.8, 0],
    ["COVID-19", "Kelelahan", 0.8, 0],
    ["COVID-19", "Sesak Nafas", 0.8, 0],
    ["COVID-19", "Ingus atau hidung tersumbat", 0.6, 0],
    ["COVID-19", "Sakit kepala", 0.6, 0],
    ["COVID-19", "Nyeri otot", 0.6, 0],
    ["COVID-19", "Sakit tenggorokan", 0.6, 0],
    ["COVID-19", "Mual", 0.6, 0],
    ["COVID-19", "Diare", 0.6, 0],
    ["COVID-19", "Menggigil", 0.4, 0],
    ["COVID-19", "Hilang rasa", 0.8, 0],
    ["COVID-19", "Hilang bau", 0.6, 0]
]


def get_kondisi(x):
    if x == "1":
        return 0.2
    elif x == "2":
        return 0.4
    elif x == "3":
        return 0.6
    elif x == "4":
        return 0.8
    elif x == "5":
        return 1
    else:
        return 0


def print_judul():
    print("#####################################################################")
    print("################ SISTEM PAKAR UNTUK DIAGNOSA PENYAKIT  ##############")
    print("################ SARS, MERS, DAN COVID-19              ##############")
    print("################ MENGGUNAKAN METODE CERTAINTY FACTOR   ##############")
    print("#####################################################################")


def print_tingkat_gejala():
    print("######################## Pilih Tingkat Gejala #######################")
    print("# Tidak              (0)    = ketik 0 / kosongkan                   #")
    print("# Ragu-ragu          (0.2)  = ketik 1                               #")
    print("# Mungkin            (0.4)  = ketik 2                               #")
    print("# Kemungkinan Besar  (0.6)  = ketik 3                               #")
    print("# Hampir Pasti       (0.8)  = ketik 4                               #")
    print("# Pasti              (1)    = ketik 5                               #")
    print("#####################################################################")


def print_line():
    print("#####################################################################")


def reset_nilai(x, a, b):
    if a != b:
        return 1
    else:
        return x+1


print_judul()
print_tingkat_gejala()

# Dapatkan data gejala yang dirasakan pasien
gejala_dipilih = []
gejala_dipilih_kondisi = []
for i in range(len(gejala)):
    jawab = input("# Apakah mengalami "+gejala[i]+" (0/1/2/3/4/5) ? ")
    if jawab != '0' and jawab in ["1", "2", "3", "4", "5"]:
        gejala_dipilih_kondisi.append([gejala[i], jawab])
        gejala_dipilih.append(gejala[i])

print_line()
print("# Gejala di pilih   :", gejala_dipilih_kondisi)

# Dapatkan data penyakit yang terpilih sesuai dengan gejala yang di rasakan
penyakit_terpilih = []
for i in range(len(rulecf)):
    for j in range(len(gejala_dipilih)):
        if (rulecf[i][1] == gejala_dipilih[j]):
            if rulecf[i][0] not in penyakit_terpilih:
                penyakit_terpilih.append(rulecf[i][0])
print("# Penyakit terpilih :", penyakit_terpilih)
print_line()

# Loop penyakit yang terpilih untuk mendapatkan nilai
# Kemudian update data list rulecf[3] sesuai dg gejala yg dirasakan Pasien (get_kondisi)
for i in range(len(penyakit_terpilih)):
    for j in range(len(rulecf)):
        if(rulecf[j][0] == penyakit_terpilih[i] and rulecf[j][1] in gejala_dipilih):
            for k in range(len(gejala_dipilih_kondisi)):
                if(gejala_dipilih_kondisi[k][0] == rulecf[j][1]):
                    # ubah data list rulecf sesuai dengan gejala yang dipliih
                    rulecf[j] = [rulecf[j][0], rulecf[j][1], rulecf[j]
                                 [2], get_kondisi(gejala_dipilih_kondisi[k][1])]


# menghitung kombinasi hasil perkalian tiap gejala
# Kombinasi hanya dapat dilakukan pada 2 atau lebih nilai CF
# Pembobotan maksimal adalah1,00
x = 1
cf = 0
nilai_peluang = 0
nps = 0
peluang = []
peluang_group_by = []
for i in range(len(rulecf)):
    x = reset_nilai(x, rulecf[i][0], rulecf[i-1][0])
    if x == 1:
        # CF  = CF Pakar * CF User
        # nilai_peluang  = cf
        cf = rulecf[i][2]*rulecf[i][3]
        nilai_peluang = rulecf[i][2]*rulecf[i][3]
        peluang.append([rulecf[i][0], cf, nilai_peluang, x])
        peluang_group_by.append([rulecf[i][0], cf, nilai_peluang, x])
    else:
        # nps = CFold-n
        # CF(h,e)g1,g2 = CFgejala1 + CFgejala2 * (1 -CFgejala1)
        # CF(h,e)old-n,g-n = CFold-n + CFgejala-n * (1 -CFold-n)
        # nilai_peluang  = nps + cf * (1-nps)
        cf = rulecf[i][2]*rulecf[i][3]
        nps = peluang[i-1][2]
        nilai_peluang = nps+cf*(1-nps)
        peluang.append([rulecf[i][0], cf, nilai_peluang, x])
        if(rulecf[i][0] == 'SARS'):
            peluang_group_by[0] = [rulecf[i][0], cf, nilai_peluang, x]
        elif(rulecf[i][0] == 'MERS'):
            peluang_group_by[1] = [rulecf[i][0], cf, nilai_peluang, x]
        else:
            peluang_group_by[2] = [rulecf[i][0], cf, nilai_peluang, x]

# KESIMPULAN Berdasarkan hasil perhitungan metode CF diperoleh hasil kombinasi CF
for i in range(len(peluang_group_by)):
    print("# Kemungkinan ", peluang_group_by[i][0],
          " = ", peluang_group_by[i][2]*100, " %")
print_line()
