import core.sp_func as sp_func
import databases.model as model
import datetime
import os
import pandas as pd


def main():
    print_judul()
    login()


def print_judul():
    print("######################################################################################################")
    print("################### SISTEM PAKAR UNTUK DIAGNOSA PENYAKIT SARS, MERS, DAN COVID-19  ###################")
    print("################################# MENGGUNAKAN METODE CERTAINTY FACTOR   ##############################")
    print("################################# KELAS : PYT-2B - KELOMPOK I   ######################################")
    print("######################################################################################################")


def hapus_diagnosa():
    id_diagnosa = input("# Hapus Data Diagnosa dengan ID :  ")
    clear_screen()
    history = model.Sql('history')
    history.del_data('id', id_diagnosa)
    print("\n")
    print_menu()


def hapus_user():
    uname = input("# Hapus Data User :  ")
    clear_screen()
    if(uname == 'admin'):
        print('User Admin tidak dapat dihapus')
        print("\n")
        print_menu()
    else:
        users = model.Sql('users')
        users.del_data('uname', uname)
        print("\n")
        print_menu()


def kosongkan_diagnosa():
    clear_screen()
    history = model.Sql('history')
    history.del_all()
    print("\n")
    print_menu()


def cari_diagnosa():
    nama = input("# Cari Berdasarkan Nama :  ")
    print("\n")
    print("######################################################################################################")
    print("###################################### Hasil Data Pencarian ##########################################")
    print("######################################################################################################")
    history = model.Sql('history')
    d_history = pd.DataFrame(history.cari_data('name', nama), columns=[
        'Id', 'Nama', 'Umur', 'Alamat', 'SARS', 'MERS', 'COVID-19', 'Gejala', 'Tanggal'])
    print(d_history)
    print("######################################################################################################")
    print("\n")
    print_menu()


def print_menu():
    print("############################################ Pilih Menu ##############################################")
    print("# Lihat Data Diagnosa     = ketik 1    # Lihat User  = Ketik 10      # List Gejala       = Ketik 20  #")
    print("# Lakukan Diagnosa        = ketik 2    # Tambah User = Ketik 11      # List Penyakit     = Ketik 21  #")
    print("# Cari Data Diagnosa      = ketik 3    # Edit User   = Ketik 12      # List Pengetahuan  = Ketik 22  #")
    print("# Hapus Data Diagnosa     = ketik 4    # Hapus User  = Ketik 13      #                               #")
    print("# Kosongkan Data Diagnosa = ketik 5    #                             #                               #")
    print("# Exit                    = ketik 0    #                             #                               #")
    print("######################################################################################################")
    menu = input("# Pilih Menu :  ")
    if menu == '1':
        clear_screen()
        print_judul()
        get_data_diagnosa()
    elif menu == '2':
        diagnosa()
    elif menu == '3':
        cari_diagnosa()
    elif menu == '4':
        hapus_diagnosa()
    elif menu == '5':
        kosongkan_diagnosa()
    elif menu == '10':
        clear_screen()
        print_judul()
        get_data_user()
    elif menu == '11':
        clear_screen()
        print_judul()
        tambah_user()
    elif menu == '12':
        clear_screen()
        print_judul()
        update_user()
    elif menu == '13':
        clear_screen()
        print_judul()
        hapus_user()
    elif menu == '20':
        clear_screen()
        print_judul()
        get_data_gejala()
    elif menu == '21':
        clear_screen()
        print_judul()
        get_data_penyakit()
    elif menu == '22':
        clear_screen()
        print_judul()
        get_data_pengetahuan()
    elif menu == '0':
        exit
    else:
        clear_screen()
        print_judul()
        print_menu()


def print_tingkat_gejala():
    print("######################################## Pilih Tingkat Gejala ########################################")
    print("# Tidak              (0)    = ketik 0 / kosongkan                                                    #")
    print("# Ragu-ragu          (0.2)  = ketik 1                                                                #")
    print("# Mungkin            (0.4)  = ketik 2                                                                #")
    print("# Kemungkinan Besar  (0.6)  = ketik 3                                                                #")
    print("# Hampir Pasti       (0.8)  = ketik 4                                                                #")
    print("# Pasti              (1)    = ketik 5                                                                #")
    print("######################################################################################################")


def clear_screen():
    os.system("cls")


def print_line():
    print("######################################################################################################")


def get_data_diagnosa():
    print("\n")
    print("######################################################################################################")
    print("############################################## Data Diagnosa #########################################")
    print("######################################################################################################")
    history = model.Sql('history')
    d_history = pd.DataFrame(history.get_data(), columns=[
        'Id', 'Nama', 'Umur', 'Alamat', 'SARS', 'MERS', 'COVID-19', 'Gejala', 'Tanggal'])
    print(d_history)
    print("######################################################################################################")
    print("\n")
    print_menu()


def get_data_user():
    print("\n")
    print("######################################################################################################")
    print("############################################## Data User     #########################################")
    print("######################################################################################################")
    users = model.Sql('users')
    d_users = pd.DataFrame(users.get_data(), columns=['Username', 'Password'])
    print(d_users)
    print("######################################################################################################")
    print("\n")
    print_menu()


def get_data_gejala():
    print("\n")
    print("######################################################################################################")
    print("############################################## Data Gejala   #########################################")
    print("######################################################################################################")
    gejala = model.Sql('gejala')
    d_gejala = pd.DataFrame(gejala.get_data(), columns=[
                            'Id', 'Gejala'])
    print(d_gejala)
    print("######################################################################################################")
    print("\n")
    print_menu()


def get_data_penyakit():
    print("\n")
    print("######################################################################################################")
    print("############################################## Data Penyakit #########################################")
    print("######################################################################################################")
    penyakit = model.Sql('penyakit')
    d_penyakit = pd.DataFrame(penyakit.get_data(), columns=[
        'Id', 'Penyakit'])
    print(d_penyakit)
    print("######################################################################################################")
    print("\n")
    print_menu()


def get_data_pengetahuan():
    print("\n")
    print("######################################################################################################")
    print("############################################ Data Pengetahuan ########################################")
    print("######################################################################################################")
    rulecf = model.Sql('rulecf')
    d_rulecf = pd.DataFrame(rulecf.get_pengetahuan(), columns=[
        'Penyakit', 'Gejala', 'CF Pakar', 'CF User'])
    print(d_rulecf)
    print("######################################################################################################")
    print("\n")
    print_menu()


def tambah_user():
    uname = input("# Username :  ")
    pwd = input("# Password :  ")
    data = {
        'uname': uname,
        'pwd': pwd
    }
    users = model.Sql('users')
    users.add_new_data(data)
    print("\n")
    print_menu()


def update_user():
    uname = input("# Username :  ")
    pwd = input("# Password :  ")
    data = {
        'pwd': pwd
    }
    users = model.Sql('users')
    users.update_data(data, 'uname', uname)
    print("\n")
    print_menu()


def login():
    ketemu = 0
    username = input("# Username :  ")
    password = input("# Password :  ")
    users = model.Sql('users')
    if(users.cek_user(username, password) > 0):
        clear_screen()
        print_judul()
        print_menu()
    else:
        print("# coba lagi ..!!!")
        login()


def diagnosa():
    clear_screen()
    print_judul()
    print_tingkat_gejala()
    nama = input("# Masukkan Nama   : ")
    umur = None
    while type(umur) != int:
        try:
            umur = int(input("# Masukkan Umur   : "))
        except:
            print("# Input Salah")
            pass

    alamat = input("# Masukkan Alamat : ")

    # Dapatkan data gejala yang dirasakan pasien
    gejala_dipilih = []
    gejala_dipilih_kondisi = []
    objgejala = model.Sql('gejala')
    datagejala = objgejala.get_data()
    for i in range(len(datagejala)):
        jawab = input("# Apakah mengalami " +
                      datagejala[i][1]+" (0/1/2/3/4/5) ? ")
        if jawab != '0' and jawab in ["1", "2", "3", "4", "5"]:
            gejala_dipilih_kondisi.append([datagejala[i][1], jawab])
            gejala_dipilih.append(datagejala[i][1])

    print_line()
    # print("# Gejala di pilih   :", gejala_dipilih_kondisi)
    print("# Gejala di pilih : ")
    df = pd.DataFrame(gejala_dipilih_kondisi, columns=[
        'Gejala', 'Tingkat Kondisi'])
    print(df)

    # Dapatkan data penyakit yang terpilih sesuai dengan gejala yang di rasakan
    penyakit_terpilih = sp_func.get_penyakit_terpilih(gejala_dipilih)
    # print("# Penyakit terpilih :", penyakit_terpilih)
    print_line()

    # Peluang Penyakit
    peluang_group_by = sp_func.get_peluang(
        gejala_dipilih, gejala_dipilih_kondisi, penyakit_terpilih)

    # KESIMPULAN Berdasarkan hasil perhitungan metode CF diperoleh hasil kombinasi CF
    for i in range(len(peluang_group_by)):
        print("# Kemungkinan ", peluang_group_by[i][0],
              " = ", peluang_group_by[i][2]*100, " %")
    print_line()

    # buat dictionary data untuk disimpan ke db
    gejala = ''
    i = 1
    for x in gejala_dipilih_kondisi:
        kondisi = sp_func.get_kondisi_name(x[1])
        if(i != len(gejala_dipilih_kondisi)):
            gejala += x[0]+':'+kondisi+','
        else:
            gejala += x[0]+':'+kondisi
        i = i+1

    data = {
        'name': nama,
        'age': umur,
        'address': alamat,
        'sars': peluang_group_by[0][2],
        'mers': peluang_group_by[1][2],
        'covid': peluang_group_by[2][2],
        'gejala': gejala,
        'tanggal': datetime.datetime.now()
    }
    # simpan ke db
    history = model.Sql('history')
    history.add_new_data(data)
    print("\n")
    print_menu()


if __name__ == '__main__':
    main()
