from flask import Flask, render_template, request, redirect, url_for, session, flash
import core.sp_func as sp_func
import databases.model as model
import datetime

app = Flask(__name__)
app.secret_key = 'DTS-PRO-2021-PYT-2B-KELOMPOK-I'


@app.route('/')
def main():
    if 'uname' in session:
        return render_template('index.html', menu='home', uname=session['uname'])
    else:
        flash("Anda tidak memiliki Akses !!!")
        return redirect(url_for('login'))


@app.route('/index')
def index():
    if 'uname' in session:
        return render_template('index.html', menu='home', uname=session['uname'])
    else:
        flash("Anda tidak memiliki Akses !!!")
        return redirect(url_for('login'))


@app.route('/penyakit')
def penyakit():
    if 'uname' in session:
        dt = model.Sql('penyakit')
        return render_template('penyakit.html', menu='penyakit', data=dt.get_data(), uname=session['uname'])
    else:
        flash("Anda tidak memiliki Akses !!!")
        return redirect(url_for('login'))


@app.route('/gejala')
def gejala():
    if 'uname' in session:
        dt = model.Sql('gejala')
        return render_template('gejala.html', menu='gejala', data=dt.get_data(), uname=session['uname'])
    else:
        flash("Anda tidak memiliki Akses !!!")
        return redirect(url_for('login'))


@app.route('/kondisi')
def kondisi():
    if 'uname' in session:
        dt = model.Sql('kondisi')
        return render_template('kondisi.html', menu='kondisi', data=dt.get_data(), uname=session['uname'])
    else:
        flash("Anda tidak memiliki Akses !!!")
        return redirect(url_for('login'))


@app.route('/pengetahuan')
def pengetahuan():
    if 'uname' in session:
        dt = model.Sql('rulecf')
        return render_template('pengetahuan.html', menu='pengetahuan', data=dt.get_pengetahuan(), uname=session['uname'])
    else:
        flash("Anda tidak memiliki Akses !!!")
        return redirect(url_for('login'))


@app.route('/diagnosa')
def diagnosa():
    if 'uname' in session:
        dt = model.Sql('history')
        return render_template('diagnosa.html', menu='diagnosa', data=dt.get_data(), uname=session['uname'])
    else:
        flash("Anda tidak memiliki Akses !!!")
        return redirect(url_for('login'))


@app.route('/diagnosa/form')
def diagnosa_form():
    if 'uname' in session:
        dt = model.Sql('gejala')
        dtkondisi = model.Sql('kondisi')
        return render_template('diagnosa_form.html', menu='diagnosa', uname=session['uname'], data=dt.get_data(), kondisi=dtkondisi.get_data())
    else:
        flash("Anda tidak memiliki Akses !!!")
        return redirect(url_for('login'))


@app.route('/diagnosa_kosongkan')
def diagnosa_kosongkan():
    if 'uname' in session:
        history = model.Sql('history')
        history.del_all()
        flash("Data dikosongkan !!!")
        return redirect(url_for('diagnosa'))
    else:
        flash("Anda tidak memiliki Akses !!!")
        return redirect(url_for('login'))


@app.route('/diagnosa_hapus')
def diagnosa_hapus():
    id = request.args.get('id')
    if(id == ''):
        flash('Id tidak boleh kosong')
        return redirect(url_for('diagnosa'))

    else:
        history = model.Sql('history')
        result = history.del_data('id', id)
        flash(result)
        return redirect(url_for('diagnosa'))


@app.route('/diagnosa_simpan', methods=['GET', 'POST'])
def diagnosa_simpan():
    if request.method == "POST":
        nama = request.form.get("nama")
        umur = request.form.get("umur")
        alamat = request.form.get("alamat")
        gejala = request.form.getlist('gejala[]')
        kondisi = request.form.getlist('kondisi[]')
        # Dapatkan data gejala yang dirasakan pasien
        gejala_dipilih = []
        gejala_dipilih_kondisi = []
        i = 0
        for rgejala in gejala:
            jawab = kondisi[i]
            if jawab != '0' and jawab in ["1", "2", "3", "4", "5"]:
                gejala_dipilih_kondisi.append([rgejala, jawab])
                gejala_dipilih.append(rgejala)
            i += 1
        penyakit_terpilih = sp_func.get_penyakit_terpilih(gejala_dipilih)
        peluang_group_by = sp_func.get_peluang(
            gejala_dipilih, gejala_dipilih_kondisi, penyakit_terpilih)

        '''
        for i in range(len(peluang_group_by)):
            print("# Kemungkinan ", peluang_group_by[i][0],
                  " = ", peluang_group_by[i][2]*100, " %")
        '''

        gejala = ''
        i = 1
        for x in gejala_dipilih_kondisi:
            kondisi = sp_func.get_kondisi_name(x[1])
            if(i != len(gejala_dipilih_kondisi)):
                gejala += x[0]+':'+kondisi+','
            else:
                gejala += x[0]+':'+kondisi
            i = i+1

        data = {
            'name': nama,
            'age': umur,
            'address': alamat,
            'sars': peluang_group_by[0][2],
            'mers': peluang_group_by[1][2],
            'covid': peluang_group_by[2][2],
            'gejala': gejala,
            'tanggal': datetime.datetime.now()
        }
        history = model.Sql('history')
        history.add_new_data(data)

        return redirect(url_for('diagnosa'))

    dt = model.Sql('gejala')
    dtkondisi = model.Sql('kondisi')
    return render_template('diagnosa_form.html', menu='diagnosa', uname=session['uname'], data=dt.get_data(), kondisi=dtkondisi.get_data())


@ app.route('/users')
def users():
    if 'uname' in session:
        dt = model.Sql('users')
        return render_template('users.html', menu='users', data=dt.get_data(), uname=session['uname'])
    else:
        flash("Anda tidak memiliki Akses !!!")
        return redirect(url_for('login'))


@app.route('/users_form')
def users_form():
    if 'uname' in session:
        data = [["", ""]]
        return render_template('users_form.html', menu='users', userid='tambah', data=data, uname=session['uname'])
    else:
        flash("Anda tidak memiliki Akses !!!")
        return redirect(url_for('login'))


@app.route('/users_simpan', methods=['GET', 'POST'])
def users_simpan():
    if request.method == "POST":
        userid = request.form.get("userid")
        uname = request.form.get("uname")
        pwd = request.form.get("pwd")

        data = {
            'uname': uname,
            'pwd': pwd
        }
        if userid == 'tambah':
            users = model.Sql('users')
            result = users.add_new_data(data)
        else:
            if(userid == 'admin'):
                dataadmin = {'pwd': pwd}
                users = model.Sql('users')
                result = users.update_data(dataadmin, 'uname', userid)
            else:
                users = model.Sql('users')
                result = users.update_data(data, 'uname', userid)
        flash(result)
        return redirect(url_for('users'))

    else:
        return redirect(url_for('users_form'))


@app.route('/users_hapus')
def users_hapus():
    uname = request.args.get('id')
    if(uname == 'admin'):
        flash('User Admin tidak dapat dihapus')
        return redirect(url_for('users'))

    else:
        users = model.Sql('users')
        result = users.del_data('uname', uname)
        flash(result)
        return redirect(url_for('users'))


@app.route('/users_edit')
def users_edit():
    uname = request.args.get('id')
    if 'uname' in session:
        users = model.Sql('users')
        print(users.cari_data('uname', uname))
        return render_template('users_form.html', menu='users', uname=session['uname'], userid=uname, data=users.cari_data('uname', uname))
    else:
        flash("Anda tidak memiliki Akses !!!")
        return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")
        users = model.Sql('users')
        if(users.cek_user(username, password) > 0):
            session['uname'] = username
            return redirect(url_for('index'))
        else:
            flash("Username atau password tidak valid !!!")
            return redirect(url_for('login'))
    else:
        return render_template('login.html')


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('login'))


if __name__ == "__main__":
    app.run(debug=True)
