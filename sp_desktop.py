from core import sp_data as sp_data, sp_func as sp_func
from databases import model as model
import tkinter as tk
from tkinter import Grid, messagebox
import datetime

class Table:      
    # class table digunakan untuk menampilkan data bentuk tabulasi
    def __init__(self,parent,header,widthHeader,height=1):
        self.parent=parent                  # parent component
        self.header = header                # list header dari table
        self.widthHeader = widthHeader      # list width dari masing-masing column
        self.height=height                  # menentukan tinggi dari setiap row

    def draw(self,data,rowGrid):        
        # code for creating table
        # data => list of data
        # rowGrid => row index untuk mulai menggambar table

        # draw header
        for j in range(len(self.header)):      #loop sebanyak jumlah elemen
            e = tk.Entry(self.parent, width=self.widthHeader[j],fg='#000',bg='#ccc',font=('Times New Roman',12,'bold'),justify='center')
            e.grid(row=rowGrid, column=j)
            e.insert('end',self.header[j])
            e.configure(state='disabled')               #set entry agar tidak bisa diclick
        
        # draw content                  
        for i in range(len(data)):      #loop sebanyak jumlah elemen
            for j in range(len(self.header)):                  #loop sebanyak panjang header                
                e = tk.Text(self.parent, width=self.widthHeader[j],fg='#000',bg='#ccc',font=('Times New Roman',12),wrap=tk.WORD,height=self.height)
                e.tag_configure("center", justify='center')
                e.grid(row=i+rowGrid+1, column=j)               # rowgrid+i+1 agar render di bawah header
                val = data[i][j]                
                e.insert('end',self.convertDecimal2Percentage(val) if type(val)==float and self.height==4 else val,'center')            #height == 4, khusus data diagnosa akibat penulisan gejala ke bawah (wrap)
                e.configure(state='disabled')                   #disable text entry
                
    def convertDecimal2Percentage(self,value):
        # convert float to percentage
        return str(round(value*100,2))+' %'        

class window(tk.Frame):
    def __init__(self,root):
        self.root = root
        self.root.title("SISTEM PAKAR PENYAKIT SARS, MERS & COVID-19")                      # set title dari window
        self.root.geometry("%dx%d+%d+%d" % (self.root.winfo_screenwidth(), self.root.winfo_screenheight()-60,-8,0)) #set ukuran windows menjadi ukuran monitor serta penentuan titik awal di (0,0)                        
        self.canvas = tk.Canvas(self.root,width=self.root.winfo_screenwidth(),height=self.root.winfo_screenheight())    #create canvas
        self.window = tk.Frame(self.canvas)
        self.scrollbar = tk.Scrollbar(self.root,command = self.canvas.yview)                #create scrollbar
        self.canvas.configure(yscrollcommand=self.scrollbar.set)
        self.window.bind("<Configure>",self.onFrameConfigure)
        self.window.pack(expand=True)
        self.scrollbar.pack(side=tk.RIGHT,fill='y')
        self.canvas.create_window(self.canvas.winfo_screenwidth()/2,self.canvas.winfo_screenheight()/2,window=self.window)          # render self.window di tengah canvas
        self.canvas.pack(expand=True)
                                
        self.setLoginPage()   #menampilkan loginpage         
    
    def onFrameConfigure(self, event):
        '''Reset the scroll region to encompass the inner frame'''
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

    def setLoginPage(self):
        # buat login page
        self.LoginFrame = tk.Frame(self.window)
        label = tk.Label(self.LoginFrame,text="Sistem Pakar Untuk Diagnosa Penyakit SARS, MERS & CoVID-19 Menggunakan Metode Certainty Factor",
            pady=30,    #padding untuk sumbu y
            font=("Times New Roman",30),
            wraplength=700      #pembatasan penulisan text dalam satu baris
        )    
        label.grid(pady=10,columnspan=3)

        labelUsername = tk.Label(self.LoginFrame,text="Username",font=("Times New Roman",15))    
        labelUsername.grid(pady=10,row=1,column=0)
        username = tk.Entry(self.LoginFrame,bg="white", width=35, justify="center",font=("Times New Roman",15))         
        username.grid(row=1,column=1)

        labelPassword = tk.Label(self.LoginFrame,text="Password",            
            font=("Times New Roman",15),            
        )    
        labelPassword.grid(pady=10,column=0,row=2)
        password = tk.Entry(self.LoginFrame,bg="white", width=35, justify="center",font=("Times New Roman",15),show="*")    #show digunakan untuk password
        password.grid(pady=8,column=1,row=2)

        loginBtn = tk.Button(self.LoginFrame,text="Login",bg="#aaa",activebackground='#ccc',width=38,font=("Times New Roman",13),command=lambda :self.loginAction(username.get(),password.get()))
        loginBtn.grid(column=1)
        self.LoginFrame.pack(expand=True)               #agar tampilan terletak di tengah
    
    def loginAction(self,username,password):        
        # action ketika tekan button loginBtn
        users = model.Sql('users')
        if(users.cek_user(username, password) > 0):
            self.LoginFrame.destroy()                                          #detroy frame login karena sudah tidak diperlukan lagi
            self.showMenuPage()
        else:
            messagebox.showerror("Gagal Login","Username atau Password Salah")  #tampilkan error message ketika gagal login
    
    def showMenuPage(self):
        # self.canvas.configure(yscrollcommand=self.scrollbar.set)    
        # menampilkan menu
        # self.canvas.yview(tk.SCROLL,1)                
        try:
            # jika komponen menu frame sudah pernah dirender sebelumnya, maka tinggal panggil pack untuk rerender
            # block dijalankan jika user masuk ke submenu lain kemudian kembali ke menu utama
            self.MenuFrame.pack(expand=True)
        except:
            # Dijalankan untuk pertama kali setelah login
            self.MenuFrame=tk.Frame(self.window)
            label = tk.Label(self.MenuFrame,text="Menu Sistem Pakar",
                pady=40,    
                font=("Times New Roman",35,'bold'),            
            )
            label.grid(pady=10,columnspan=3)            #columnspan 3 agar terletak ditengah diantara 3 sub menu

            historyFrame=tk.Frame(self.MenuFrame)            #frame untuk kelompok history data
            historyLabel = tk.Label(historyFrame,text="History Section",
                pady=1,
                font=("Times New Roman",14,'bold'),
            )        
            historyLabel.grid(pady=10,columnspan=3)            #columnspan 3 agar terletak ditengah diantara 3 sub menu
            showHistoryBtn = tk.Button(historyFrame,text="Lihat Data Diagnosa",bg="#aaa",activebackground='#ccc',width=25,font=("Times New Roman",13),command=self.showHistoryAction)
            showHistoryBtn.grid(pady=3)
            addHistoryBtn = tk.Button(historyFrame,text="Diagnosa Baru",bg="#aaa",activebackground='#ccc',width=25,font=("Times New Roman",13),command=self.addNewHistoryAction)
            addHistoryBtn.grid(pady=3)
            findHistoryBtn = tk.Button(historyFrame,text="Cari Data Diagnosa",bg="#aaa",activebackground='#ccc',width=25,font=("Times New Roman",13),command=self.findHistoryAction)
            findHistoryBtn.grid(pady=3)
            removeHistoryBtn = tk.Button(historyFrame,text="Hapus Data Diagnosa",bg="#aaa",activebackground='#ccc',width=25,font=("Times New Roman",13),command=self.removeAction)
            removeHistoryBtn.grid(pady=3)
            emptyHistoryBtn = tk.Button(historyFrame,text="Kosongkan Data Diagnosa",bg="#aaa",activebackground='#ccc',width=25,font=("Times New Roman",13),command=self.emptyHistoryAction)
            emptyHistoryBtn.grid(pady=3)
            exitBtn = tk.Button(historyFrame,text="Keluar",bg="#aaa",activebackground='#ccc',width=25,font=("Times New Roman",13),command=self.exitAction)
            exitBtn.grid(pady=3)
            historyFrame.grid(row=1,column=0,padx=30,sticky='N')
            
            # kelompok user menu
            userFrame=tk.Frame(self.MenuFrame)        
            userLabel = tk.Label(userFrame,text="User Section",
                pady=1,
                font=("Times New Roman",14,'bold'),
            )        
            userLabel.grid(pady=10,columnspan=3)            #columnspan 3 agar terletak ditengah diantara 3 sub menu
            showUserBtn = tk.Button(userFrame,text="Lihat User",bg="#aaa",activebackground='#ccc',width=25,font=("Times New Roman",13),command=self.showUserAction)
            showUserBtn.grid(pady=3)
            addUserBtn = tk.Button(userFrame,text="Tambah User",bg="#aaa",activebackground='#ccc',width=25,font=("Times New Roman",13),command=lambda:self.UserAction('add'))   #penggunaan lambda agar bisa kirim parameter ke dalam function
            addUserBtn.grid(pady=3)
            editUserBtn = tk.Button(userFrame,text="Edit User",bg="#aaa",activebackground='#ccc',width=25,font=("Times New Roman",13),command=self.UserAction)
            editUserBtn.grid(pady=3)
            removeUserBtn = tk.Button(userFrame,text="Hapus User",bg="#aaa",activebackground='#ccc',width=25,font=("Times New Roman",13),command=lambda:self.removeAction('user'))
            removeUserBtn.grid(pady=3)        
            userFrame.grid(row=1,column=1,padx=30,sticky='N')
            
            # kelompok pengetahuan menu
            knowledgeFrame=tk.Frame(self.MenuFrame)        
            knowledgeLabel = tk.Label(knowledgeFrame,text="Pengetahuan Section",
                pady=1,
                font=("Times New Roman",14,'bold'),
            )        
            knowledgeLabel.grid(pady=10,columnspan=3)            #columnspan 3 agar terletak ditengah diantara 3 sub menu
            gejalaInfoBtn = tk.Button(knowledgeFrame,text="Lihat Gejala",bg="#aaa",activebackground='#ccc',width=25,font=("Times New Roman",13),command=self.gejalaAction)
            gejalaInfoBtn.grid(pady=3)
            penyakitInfoBtn = tk.Button(knowledgeFrame,text="Lihat Penyakit",bg="#aaa",activebackground='#ccc',width=25,font=("Times New Roman",13),command=self.penyakitAction)
            penyakitInfoBtn.grid(pady=3)
            knowledgeInfoBtn = tk.Button(knowledgeFrame,text="Lihat Pengetahuan",bg="#aaa",activebackground='#ccc',width=25,font=("Times New Roman",13),command=self.pengetahuanAction)
            knowledgeInfoBtn.grid(pady=3)        
            knowledgeFrame.grid(row=1,column=2,padx=30,sticky='N')

            self.MenuFrame.pack(expand=True)            
    
    def showHistoryAction(self):
        # action user click lihat data diagnosa
        self.MenuFrame.pack_forget() #hide menu frame
        historyListFrame = tk.Frame(self.window)
        label = tk.Label(historyListFrame,text="Data Diagnosa",
            pady=30,            
            font=("Times New Roman",30,'bold'),
        )
        label.grid(pady=10, columnspan=10)

        tabel = Table(historyListFrame,['ID', 'Name', 'Umur', 'Alamat', 'SARS', 'MERS', 'COVID-19','Gejala','Tanggal'],[4,15,5,25,10,10,10,30,15],4)  #parent, list header table , list width masing-masing column 
        listDiagnosa = model.Sql('history').get_data()        
        tabel.draw(listDiagnosa,1)      # draw data history ke dalam table start di row 1

        cancelBtn = tk.Button(historyListFrame,text="Kembali Ke Menu",bg="#aaa",activebackground='#ccc',font=("Times New Roman",13),command=lambda:[historyListFrame.destroy(),self.cancelAction()])     # lambda:[x,y] -> menjalankan dua fungsi sequence
        cancelBtn.grid(pady=20,padx=5,columnspan=10)
        
        historyListFrame.pack(expand=True)

    def addNewHistoryAction(self):
        self.MenuFrame.pack_forget() #hide menu frame
        self.canvas.yview_moveto(0)
        self.HistoryFormFrame = tk.Frame(self.window)
        label = tk.Label(self.HistoryFormFrame,text="Diagnosa Penyakit",
            pady=30,            
            font=("Times New Roman",30,'bold'),            
        )
        label.grid(pady=10, column=0, columnspan=4)

        # isi identitas pasien
        labelName = tk.Label(self.HistoryFormFrame,text="Nama",
            font=("Times New Roman",15),
        )
        labelName.grid(row=3,column=0,padx=15,sticky=tk.W)
        
        entryName = tk.Entry(self.HistoryFormFrame,bg="white", width=30, justify="center",font=("Times New Roman",15))
        entryName.grid(row=3,column=1)
        self.HistoryFormFrame.pack(expand=True)
        
        labelAge = tk.Label(self.HistoryFormFrame,text="Umur",
            font=("Times New Roman",15),
            pady=5            
        )
        labelAge.grid(row=4,column=0,padx=15,sticky=tk.W)
        
        entryAge = tk.Entry(self.HistoryFormFrame,bg="white", width=30, justify="center",font=("Times New Roman",15))         
        entryAge.grid(row=4,column=1)

        labelAlamat = tk.Label(self.HistoryFormFrame,text="Alamat",
            font=("Times New Roman",15),
            pady=5            
        )
        labelAlamat.grid(row=5,column=0,padx=15,sticky=tk.W)
        
        entryAlamat = tk.Entry(self.HistoryFormFrame,bg="white", width=30, justify="center",font=("Times New Roman",15))
        entryAlamat.grid(row=5,column=1)

        label = tk.Label(self.HistoryFormFrame,text="Apakah Anda Mengalami ",
            pady=30,            
            font=("Times New Roman",12,'bold'),
        )
        label.grid(pady=5)
        # isi gejala pasien
        gejala_dipilihEntry=[]        
        # loop sebanyak list gejala
        for i in range(len(sp_data.gejala)):            
            tk.Label(self.HistoryFormFrame,text=sp_data.gejala[i],fg='#000',font=('Times New Roman',12),justify='center').grid(row=i+7,column=0,sticky='W',ipadx=10)

            # variable digunakan sebagai nilai default dan pointer terhadap value option menu
            globals()['variable%s' % i] = tk.StringVar(self.HistoryFormFrame)
            globals()['variable%s' % i].set("Tidak")
            tk.OptionMenu(self.HistoryFormFrame, globals()['variable%s' % i],'Tidak','Ragu-ragu',"Mungkin",'Kemungkinan Besar','Hampir Pasti','Pasti').grid(row=i+7,column=1,padx=5,sticky='E')            

        # penambahan btn frame agar button cancel dan save berada di area yang sana
        btnFrame = tk.Frame(self.HistoryFormFrame)        
        cancelBtn = tk.Button(btnFrame,text="Kembali",bg="#aaa",activebackground='#ccc',width=10,font=("Times New Roman",13),command=lambda:[self.HistoryFormFrame.destroy(),self.cancelAction()])
        cancelBtn.grid(pady=10,row=i+8,column=1)
        saveBtn = tk.Button(btnFrame,text="Simpan",bg="#aaa",activebackground='#ccc',width=10,font=("Times New Roman",13),command=lambda:[self.saveNewData(entryName.get(),entryAge.get(),entryAlamat.get(),sp_data.gejala)])
        saveBtn.grid(pady=10,row=i+8,column=2,padx=5)
        btnFrame.grid(column=1,sticky="E")     #sticky agar component align right (east)                          

        self.HistoryFormFrame.pack(expand=True)                  

    def saveNewData(self,nama,umur,alamat,datagejala):        
        # fungsi untuk melakukan penghitungan nilai peluang ketiga penyakit

        gejalaEntry=[]
        gejalaEntryKode=[]
        #mengkonversi gejala yang dipilih agar terbaca di module sp_func.py fungsi get_kondisi
        jawab=[]
        for i in range(len(datagejala)):
            gejalaEntry.append(i)
            gejalaEntryKode.append(i)
            jawab.append(i)
            if globals()['variable%s' % i].get() == "Ragu-ragu":
                jawab[i]=("1")
            elif globals()['variable%s' % i] == "Mungkin":
                jawab[i]=("2")
            elif globals()['variable%s' % i].get() == "Kemungkinan Besar":
                jawab[i]=("3")
            elif globals()['variable%s' % i].get() == "Hampir Pasti":
                jawab[i]=("4")
            elif globals()['variable%s' % i].get() == "Pasti":
                jawab[i]=("5")
            else:
                jawab[i]=("0")
  
            gejalaEntry[i]=[datagejala[i],globals()['variable%s' % i].get()]


        # block jika nama atau alamat kosong
        if nama=='' or alamat=='':
            messagebox.showerror('Data Kosong', 'Data Tidak Boleh Kosong')
            return
        # block jika input umur bukan int
        try:
            umur=int(umur)
        except:
            messagebox.showerror('Data Umur TIdak Sesuai', 'Format Data Umur Yang Anda Masukkan Salah')
            return
        
        gejala_dipilih=[]
        gejala_dipilih_kondisi=[]
        gejala_dipilih_kondisi=[]
        gejala=''
        # gejala entry-> array 2 dimensi ---> gejala entry[x][y]
        # indek 0 (x), nama gejala
        # indek 1 (y), bobot gejala
        # lihat sp_console
        for i in range(len(sp_data.gejala)):
            if gejalaEntry[i][1] !='Tidak':
                gejala_dipilih.append(gejalaEntry[i][0])
                gejala_dipilih_kondisi.append([sp_data.gejala[i],jawab[i]])
                gejala+=gejalaEntry[i][0]+' : '+gejalaEntry[i][1]+'\n'
        
        penyakit_terpilih = sp_func.get_penyakit_terpilih(gejala_dipilih)
        peluang_group_by = sp_func.get_peluang(gejala_dipilih, gejala_dipilih_kondisi, penyakit_terpilih)
        
        data = {
            'name': nama,
            'age': umur,
            'address': alamat,
            'sars': peluang_group_by[0][2],
            'mers': peluang_group_by[1][2],
            'covid': peluang_group_by[2][2],
            'gejala': gejala,
            'tanggal': datetime.datetime.now()
        }
        # simpan ke db
        history = model.Sql('history')
        history.add_new_data(data)
        message=""
        for i in range(len(peluang_group_by)):            
            message += "Kemungkinan "+ str(peluang_group_by[i][0])+ " = "+str(peluang_group_by[i][2]*100)+ " %\n"

        messagebox.showinfo('Hasil Perhitungan',message)
        self.HistoryFormFrame.destroy()
        self.showMenuPage()                                    

    def findHistoryAction(self):
        self.MenuFrame.pack_forget() #hide menu frame
        self.searchFrame = tk.Frame(self.window)        
        label = tk.Label(self.searchFrame,text="Cari Data Diagnosa",
            pady=30,            
            font=("Times New Roman",30),            
        )
        label.grid(pady=10, column=0, columnspan=10)
    
        searchboxFrame = tk.Frame(self.searchFrame)        #frame untuk input box dan button, hide setelah click cari        
        labelName = tk.Label(searchboxFrame,text="Masukkan Nama",
            font=("Times New Roman",15),
        )
        labelName.grid(row=3,column=0,padx=15)
        
        keyword = tk.Entry(searchboxFrame,bg="white", width=30, justify="center",font=("Times New Roman",15))
        keyword.grid(row=3,column=1)
        searchboxFrame.grid()
        btnFrame = tk.Frame(searchboxFrame)
        cancelBtn = tk.Button(btnFrame,text="Kembali",bg="#aaa",activebackground='#ccc',width=10,font=("Times New Roman",13),command=lambda:[self.searchFrame.destroy(),self.cancelAction()])
        cancelBtn.grid(pady=10,row=1,column=0,padx=5)
        searchBtn = tk.Button(btnFrame,text="Cari",bg="#aaa",activebackground='#ccc',width=10,font=("Times New Roman",13),command=lambda:[searchboxFrame.grid_remove(),self.searchHistoryAction(keyword.get())])
        searchBtn.grid(pady=10,row=1,column=1)
        btnFrame.grid(column=1,columnspan=3,sticky="E")     #sticky agar component align right (east)                        
        self.searchFrame.pack()

    def searchHistoryAction(self,keyword):
        history = model.Sql('history')
        hasil=history.cari_data('name',keyword)
        if len(hasil)==0:            
            messagebox.showerror("Data Not Found","Data dengan Nama"+keyword+"TIdak Ditemukan")
        else:            
            label = tk.Label(self.searchFrame,text="Hasil Pencarian",
                pady=5,
                font=("Times New Roman",15,'bold'),                
            )
            label.grid(pady=10, row= 4,sticky='W',columnspan=7)
            label = tk.Label(self.searchFrame,text="Keyword : "+ keyword,                
                font=("Times New Roman",15),                
            )
            label.grid(pady=10,sticky='W',columnspan=7)
            frameTabel = tk.Frame(self.searchFrame)            
            table = Table(self.searchFrame,['ID', 'Name', 'Umur', 'Alamat', 'SARS', 'MERS', 'COVID-19','Gejala','Tanggal'],[4,15,5,25,10,10,10,30,15],4)
            table.draw(hasil,7)
            frameTabel.grid()
            cancelBtn = tk.Button(self.searchFrame,text="Kembali Ke Menu",bg="#aaa",activebackground='#ccc',font=("Times New Roman",13),command=lambda:[self.searchFrame.destroy(),self.cancelAction()])
            cancelBtn.grid(pady=20,padx=5,columnspan=10)                                

    def cancelAction(self):                
        self.showMenuPage()
    
    def emptyHistoryAction(self):
        value = messagebox.askokcancel("Konfirmasi","Apakah Anda Benar - benar ingin menghapus seluruh Data History ?")
        if value:
            history = model.Sql('history')
            messagebox.showinfo("Data telah Dihapus", "Data sebanyak "+str(history.del_all())+" telah berhasil dihapus dari table history")

    def exitAction(self):
        self.root.destroy()

    def showUserAction(self):
        self.MenuFrame.pack_forget() #hide menu frame
        userListFrame = tk.Frame(self.window)
        label = tk.Label(userListFrame,text="Data User",
            pady=30,            
            font=("Times New Roman",30,'bold'),
        )
        label.grid(pady=10, columnspan=4)

        tabel = Table(userListFrame,['Username'],[30])
        listUser = model.Sql('users').get_data()                
        tabel.draw(listUser,3)      # draw data history ke dalam table

        cancelBtn = tk.Button(userListFrame,text="Kembali Ke Menu",bg="#aaa",activebackground='#ccc',font=("Times New Roman",13),command=lambda:[userListFrame.destroy(),self.cancelAction()])
        cancelBtn.grid(pady=20,padx=5,columnspan=3)
        
        userListFrame.pack(expand=True)

    def UserAction(self,type="update"):
        # fitur untuk edit dan tambah user
        self.MenuFrame.pack_forget() #hide menu frame

        self.userFrame = tk.Frame(self.window)
        label = tk.Label(self.userFrame,text= "Tambah Data User" if type=="add" else "Edit Data User",
            pady=30,            
            font=("Times New Roman",30,'bold'),            
        )
        label.grid(pady=10, column=0, columnspan=4)

        # isi identitas pasien
        labelUsername = tk.Label(self.userFrame,text="Username",
            font=("Times New Roman",15),
        )
        labelUsername.grid(row=3,column=0,padx=15,sticky=tk.W)
        
        entryUsername = tk.Entry(self.userFrame,bg="white", width=30, justify="center",font=("Times New Roman",15))
        entryUsername.grid(row=3,column=1)        
        
        labelPassword = tk.Label(self.userFrame,text="Password",
            font=("Times New Roman",15),
            pady=5            
        )
        labelPassword.grid(row=4,column=0,padx=15,sticky=tk.W)
        
        entryPassword = tk.Entry(self.userFrame,bg="white", width=30, justify="center",font=("Times New Roman",15),show="*")         
        entryPassword.grid(row=4,column=1)

        labelKonfirmasiPassword = tk.Label(self.userFrame,text="Konfirmasi Password",
            font=("Times New Roman",15),
            pady=5            
        )
        labelKonfirmasiPassword.grid(row=5,column=0,padx=15,sticky=tk.W)
        
        entryKonfirmasiPassword = tk.Entry(self.userFrame,bg="white", width=30, justify="center",font=("Times New Roman",15),show="*")         
        entryKonfirmasiPassword.grid(row=5,column=1)

        btnFrame = tk.Frame(self.userFrame)        
        cancelBtn = tk.Button(btnFrame,text="Kembali",bg="#aaa",activebackground='#ccc',width=10,font=("Times New Roman",13),command=lambda:[self.userFrame.destroy(),self.cancelAction()])
        cancelBtn.grid(pady=10,row=6,column=1)
        saveBtn = tk.Button(btnFrame,text="Simpan" if type=="add" else "Update",bg="#aaa",activebackground='#ccc',width=10,font=("Times New Roman",13),command=lambda:[self.saveorUpdateUser(type,entryUsername.get(),entryPassword.get(),entryKonfirmasiPassword.get())])
        saveBtn.grid(pady=10,row=6,column=2,padx=5)
        btnFrame.grid(column=1,sticky="E")     #sticky agar component align right (east)                          

        self.userFrame.pack(expand=True)
        
    def saveorUpdateUser(self,type,name,password,konfirmasiPassword):
        users = model.Sql('users')        
        if type=='update':            
            if len(users.cari_data('uname',name))==0:
                messagebox.showerror("Data Not Found","Data dengan Username "+name+" Tidak Ditemukan")
                return

        if password=='' or password!=konfirmasiPassword:
            messagebox.showerror("Password Beda","Password dan Konfirmasi Password Berbeda")
            return
        
        if type == 'update':                   
            users.update_data({'pwd': password}, 'uname', name)
        
        else:                  
            users.add_new_data({'uname': name,'pwd': password})
        
        messagebox.showinfo("Operasi Berhasil",("Edit" if type=='update' else 'Tambah')+" Data User Telah Berhasil")        
        self.userFrame.destroy()
        self.showMenuPage()        

    def removeAction(self,type="history"):
        # remove data historu ataupun user
        self.MenuFrame.pack_forget() #hide menu frame
        self.removeFrame = tk.Frame(self.window)
        label = tk.Label(self.removeFrame,text="Hapus Data Diagnosa" if type=='history' else "Hapus Data User",
            pady=30,            
            font=("Times New Roman",30),            
        )
        label.grid(pady=10, column=0, columnspan=2)
        labelName = tk.Label(self.removeFrame,text="Masukkan ID History" if type=='history' else 'Masukkan Username',
            font=("Times New Roman",15),
        )
        labelName.grid(row=3,column=0,padx=15)
        keyword = tk.Entry(self.removeFrame,bg="white", width=30, justify="center",font=("Times New Roman",15))
        keyword.grid(row=3,column=1)
        btnFrame = tk.Frame(self.removeFrame)
        cancelBtn = tk.Button(btnFrame,text="Cancel",bg="#aaa",activebackground='#ccc',width=10,font=("Times New Roman",13),command=lambda:[self.removeFrame.destroy(),self.cancelAction()])
        cancelBtn.grid(pady=10,row=1,column=0,padx=5)
        searchBtn = tk.Button(btnFrame,text="Hapus Data",bg="#aaa",activebackground='#ccc',width=10,font=("Times New Roman",13),command=lambda: self.removeDataAction(type,keyword.get()))
        searchBtn.grid(pady=10,row=1,column=1)
        btnFrame.grid(column=1,columnspan=3,sticky="E")
        self.removeFrame.pack() 

    def removeDataAction(self,type,keyword):
        # remove Data Action ke db untuk hapus data history dan data user
        if type=='history':
            history = model.Sql('history')            
            if len(history.cari_data('id',keyword))==0:
                # data dengan id keyword tidak ditemukan, show error
                messagebox.showerror("Data Not Found","Data History dengan Id "+keyword+" Tidak Ditemukan")
                return

            history.del_data('id',keyword)     #data ditemukan, lalu hapus
        else:
            if keyword == 'admin':
                messagebox.showerror("Operation Failed","User Admin Tidak Dapat Dihapus")
                return
            else:
                users = model.Sql('users')                
                if len(users.cari_data('uname',keyword))==0:
                    messagebox.showerror("Data Not Found","Data dengan Username "+keyword+" Tidak Ditemukan")
                    return
                users.del_data('uname', keyword)
        
        messagebox.showinfo("Data telah Dihapus","Data dengan "+('id : ' if type=='history' else 'username : ')+keyword+" telah berhasil dihapus")
        self.removeFrame.destroy()
        self.showMenuPage()

    def gejalaAction(self):
        # action gejala click lihat data gejala
        self.MenuFrame.pack_forget() #hide menu frame
        gejalaListFrame = tk.Frame(self.window)
        label = tk.Label(gejalaListFrame,text="Data Gejala",
            pady=30,            
            font=("Times New Roman",30,'bold'),
        )
        label.grid(pady=10, columnspan=5)

        tabel = Table(gejalaListFrame,['Id', 'Gejala'],[10,20])  #parent, list header table , list width masing-masing column 
        listGejala = model.Sql('gejala').get_data()        
        
        tabel.draw(listGejala,1)      # draw data gejala ke dalam table start di row 1

        cancelBtn = tk.Button(gejalaListFrame,text="Kembali Ke Menu",bg="#aaa",activebackground='#ccc',font=("Times New Roman",13),command=lambda:[gejalaListFrame.destroy(),self.cancelAction()])     # lambda:[x,y] -> menjalankan dua fungsi sequence
        cancelBtn.grid(pady=20,padx=5,columnspan=10)
        
        gejalaListFrame.pack(expand=True)

    def penyakitAction(self):
        # action Penyakit click lihat data Penyakit
        self.MenuFrame.pack_forget() #hide menu frame
        penyakitListFrame = tk.Frame(self.window)
        label = tk.Label(penyakitListFrame,text="Data Penyakit",
            pady=30,            
            font=("Times New Roman",30,'bold'),
        )
        label.grid(pady=10, columnspan=10)

        tabel = Table(penyakitListFrame,['Id', 'Penyakit'],[10,20])  #parent, list header table , list width masing-masing column 
                     
        listPenyakit = model.Sql('penyakit').get_data()        
        tabel.draw(listPenyakit,1)      # draw data penyakit ke dalam table start di row 1

        cancelBtn = tk.Button(penyakitListFrame,text="Kembali Ke Menu",bg="#aaa",activebackground='#ccc',font=("Times New Roman",13),command=lambda:[penyakitListFrame.destroy(),self.cancelAction()])     # lambda:[x,y] -> menjalankan dua fungsi sequence
        cancelBtn.grid(pady=20,padx=5,columnspan=10)
        
        penyakitListFrame.pack(expand=True)

    def pengetahuanAction(self):
        # action Pengetahuan click lihat data Pengetahuan
        self.MenuFrame.pack_forget() #hide menu frame
        pengetahuanListFrame = tk.Frame(self.window)
        label = tk.Label(pengetahuanListFrame,text="Data Pengetahuan",
            pady=30,            
            font=("Times New Roman",30,'bold'),
        )
        label.grid(pady=10, columnspan=10)

        tabel = Table(pengetahuanListFrame,['Penyakit', 'Gejala', 'CF Pakar'],[10,20,15])  #parent, list header table , list width masing-masing column 
        listPengetahuan = model.Sql('rulecf').get_pengetahuan()        
        tabel.draw(listPengetahuan,1)      # draw data gejala ke dalam table start di row 1

        cancelBtn = tk.Button(pengetahuanListFrame,text="Kembali Ke Menu",bg="#aaa",activebackground='#ccc',font=("Times New Roman",13),command=lambda:[pengetahuanListFrame.destroy(),self.cancelAction()])     # lambda:[x,y] -> menjalankan dua fungsi sequence
        cancelBtn.grid(pady=20,padx=5,columnspan=10)
        
        pengetahuanListFrame.pack(expand=True)

if __name__ == '__main__':
    root = tk.Tk()
    my_gui = window(root)
    root.mainloop()
