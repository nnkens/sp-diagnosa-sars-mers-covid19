import databases.model as model


def get_kondisi(x):
    if x == "1":
        return 0.2
    elif x == "2":
        return 0.4
    elif x == "3":
        return 0.6
    elif x == "4":
        return 0.8
    elif x == "5":
        return 1
    else:
        return 0


def get_kondisi_name(x):
    if x == "1":
        return 'Ragu-ragu'
    elif x == "2":
        return 'Mungkin'
    elif x == "3":
        return 'Kemungkinan Besar'
    elif x == "4":
        return 'Hampir Pasti'
    elif x == "5":
        return 'Pasti'
    else:
        return 'Tidak'


def reset_nilai(x, a, b):
    if a != b:
        return 1
    else:
        return x+1

# Dapatkan data penyakit yang terpilih sesuai dengan gejala yang di rasakan
# Get Penyakit terpilih


def get_penyakit_terpilih(gejala_dipilih):
    penyakit_terpilih = []
    rulecf = model.Sql('rulecf')
    pengetahuan = rulecf.get_pengetahuan()
    for i in range(len(pengetahuan)):
        for j in range(len(gejala_dipilih)):
            if (pengetahuan[i][1] == gejala_dipilih[j]):
                if pengetahuan[i][0] not in penyakit_terpilih:
                    penyakit_terpilih.append(pengetahuan[i][0])
    return penyakit_terpilih


# Loop penyakit yang terpilih untuk mendapatkan nilai
# Kemudian update data list rulecf[3] sesuai dg gejala yg dirasakan Pasien (get_kondisi)
def get_peluang(gejala_dipilih, gejala_dipilih_kondisi, penyakit_terpilih):
    rulecf = model.Sql('rulecf')
    pengetahuan = rulecf.get_pengetahuan()
    for i in range(len(penyakit_terpilih)):
        for j in range(len(pengetahuan)):
            if(pengetahuan[j][0] == penyakit_terpilih[i] and pengetahuan[j][1] in gejala_dipilih):
                for k in range(len(gejala_dipilih_kondisi)):
                    if(gejala_dipilih_kondisi[k][0] == pengetahuan[j][1]):
                        # ubah data list rulecf sesuai dengan gejala yang dipliih
                        pengetahuan[j] = [pengetahuan[j][0], pengetahuan[j][1],
                                          pengetahuan[j][2], get_kondisi(gejala_dipilih_kondisi[k][1])]

    # menghitung kombinasi hasil perkalian tiap gejala
    # Kombinasi hanya dapat dilakukan pada 2 atau lebih nilai CF
    # Pembobotan maksimal adalah1,00
    x = 1
    cf = 0
    nilai_peluang = 0
    nps = 0
    peluang = []
    peluang_group_by = []
    for i in range(len(pengetahuan)):
        x = reset_nilai(x, pengetahuan[i][0], pengetahuan[i-1][0])
        if x == 1:
            # CF  = CF Pakar * CF User
            # nilai_peluang  = cf
            cf = pengetahuan[i][2]*pengetahuan[i][3]
            nilai_peluang = pengetahuan[i][2]*pengetahuan[i][3]
            peluang.append([pengetahuan[i][0], cf, nilai_peluang, x])
            peluang_group_by.append(
                [pengetahuan[i][0], cf, nilai_peluang, x])

            #print(pengetahuan[i][0], cf, nilai_peluang, x)
        else:
            # nps = CFold-n
            # CF(h,e)g1,g2 = CFgejala1 + CFgejala2 * (1 -CFgejala1)
            # CF(h,e)old-n,g-n = CFold-n + CFgejala-n * (1 -CFold-n)
            # nilai_peluang  = nps + cf * (1-nps)
            cf = pengetahuan[i][2]*pengetahuan[i][3]
            nps = peluang[i-1][2]
            nilai_peluang = nps+cf*(1-nps)
            peluang.append([pengetahuan[i][0], cf, nilai_peluang, x])

            #print(pengetahuan[i][0], cf, nilai_peluang, x)
            if(pengetahuan[i][0] == 'SARS'):
                peluang_group_by[0] = [
                    pengetahuan[i][0], cf, nilai_peluang, x]
            elif(pengetahuan[i][0] == 'MERS'):
                peluang_group_by[1] = [
                    pengetahuan[i][0], cf, nilai_peluang, x]
            else:
                peluang_group_by[2] = [
                    pengetahuan[i][0], cf, nilai_peluang, x]
    return peluang_group_by
