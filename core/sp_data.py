kondisi = [
    ["0", "Tidak", 0],
    ["1", "Ragu-ragu", 0.2],
    ["2", "Mungkin", 0.4],
    ["3", "Kemungkinan Besar", 0.6],
    ["4", "Hampir Pasti", 0.8],
    ["5", "Pasti", 1],
]
penyakit = ["SARS", "MERS", "COVID-19"]
gejala = ["Demam", "Batuk", "Kelelahan", "Sesak Nafas", "Rasa tidak enak", "Sakit dan nyeri tubuh", "Sakit kepala", "Menggigil", "Nyeri otot",
          "Ingus atau hidung tersumbat", "Sakit tenggorokan", "Mual", "Diare", "Hilang rasa", "Hilang bau", "Batuk tidak berdahak", "Batuk berdarah"]

# rulecf[0]: Penyakit
# rulecf[1]: Gejala
# rulecf[2]: CF Pakar (Data Skala nilai bobot yang diberikan oleh Pakar)
# rulecf[3]: CF User  (Gejala yang dirasakan Pasien)
rulecf = [
    ["SARS", "Demam", 0.8, 0],
    ["SARS", "Batuk", 0.8, 0],
    ["SARS", "Rasa tidak enak", 0.8, 0],
    ["SARS", "Sakit dan nyeri tubuh", 0.8, 0],
    ["SARS", "Sakit kepala", 0.8, 0],
    ["SARS", "Sesak Nafas", 0.8, 0],
    ["SARS", "Menggigil", 0.8, 0],
    ["SARS", "Diare", 0.6, 0],
    ["SARS", "Batuk tidak berdahak", 0.4, 0],
    ["MERS", "Demam", 0.8, 0],
    ["MERS", "Batuk", 0.8, 0],
    ["MERS", "Menggigil", 0.8, 0],
    ["MERS", "Nyeri otot", 0.8, 0],
    ["MERS", "Sesak Nafas", 0.6, 0],
    ["MERS", "Batuk berdarah", 0.4, 0],
    ["MERS", "Mual", 0.6, 0],
    ["MERS", "Diare", 0.6, 0],
    ["COVID-19", "Demam", 0.8, 0],
    ["COVID-19", "Batuk", 0.8, 0],
    ["COVID-19", "Kelelahan", 0.8, 0],
    ["COVID-19", "Sesak Nafas", 0.8, 0],
    ["COVID-19", "Ingus atau hidung tersumbat", 0.6, 0],
    ["COVID-19", "Sakit kepala", 0.6, 0],
    ["COVID-19", "Nyeri otot", 0.6, 0],
    ["COVID-19", "Sakit tenggorokan", 0.6, 0],
    ["COVID-19", "Mual", 0.6, 0],
    ["COVID-19", "Diare", 0.6, 0],
    ["COVID-19", "Menggigil", 0.4, 0],
    ["COVID-19", "Hilang rasa", 0.8, 0],
    ["COVID-19", "Hilang bau", 0.6, 0]
]
