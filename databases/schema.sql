CREATE TABLE history ( 
    id INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,
    name           VARCHAR(50)    NOT NULL,
    age            INTEGER     NOT NULL,
    address        VARCHAR(100),
    sars           REAL,
    mers           REAL,
    covid          REAL,
    gejala         TEXT,
    tanggal        timestamp
);

CREATE TABLE users ( 
    uname      VARCHAR(50)  PRIMARY KEY  NOT NULL,
    pwd        VARCHAR(50)    NOT NULL
);

INSERT INTO users (uname,pwd) VALUES ('admin','admin');

CREATE TABLE kondisi ( 
    kode      CHAR(1)  PRIMARY KEY  NOT NULL,
    nama      VARCHAR(50)  NOT NULL,
    nilai     REAL  NOT NULL
);

INSERT INTO kondisi (kode,nama,nilai) VALUES ('0','Tidak',0);
INSERT INTO kondisi (kode,nama,nilai) VALUES ('1','Ragu-ragu',0.2);
INSERT INTO kondisi (kode,nama,nilai) VALUES ('2','Mungkin',0.4);
INSERT INTO kondisi (kode,nama,nilai) VALUES ('3','Kemungkinan Besar',0.6);
INSERT INTO kondisi (kode,nama,nilai) VALUES ('4','Hampir Pasti',0.8);
INSERT INTO kondisi (kode,nama,nilai) VALUES ('5','Pasti',1);

CREATE TABLE gejala ( 
    id INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,
    nama      VARCHAR(50)  NOT NULL
);
INSERT INTO gejala (id,nama) VALUES (1,'Demam');
INSERT INTO gejala (id,nama) VALUES (2,'Batuk');
INSERT INTO gejala (id,nama) VALUES (3,'Kelelahan');
INSERT INTO gejala (id,nama) VALUES (4,'Sesak Nafas');
INSERT INTO gejala (id,nama) VALUES (5,'Rasa tidak enak');
INSERT INTO gejala (id,nama) VALUES (6,'Sakit dan nyeri tubuh');
INSERT INTO gejala (id,nama) VALUES (7,'Sakit kepala');
INSERT INTO gejala (id,nama) VALUES (8,'Menggigil');
INSERT INTO gejala (id,nama) VALUES (9,'Nyeri otot');
INSERT INTO gejala (id,nama) VALUES (10,'Ingus atau hidung tersumbat');
INSERT INTO gejala (id,nama) VALUES (11,'Sakit tenggorokan');
INSERT INTO gejala (id,nama) VALUES (12,'Mual');
INSERT INTO gejala (id,nama) VALUES (13,'Diare');
INSERT INTO gejala (id,nama) VALUES (14,'Hilang rasa');
INSERT INTO gejala (id,nama) VALUES (15,'Hilang bau');
INSERT INTO gejala (id,nama) VALUES (16,'Batuk tidak berdahak');
INSERT INTO gejala (id,nama) VALUES (17,'Batuk berdarah');

CREATE TABLE penyakit ( 
    id INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,
    nama      VARCHAR(50)  NOT NULL
);
INSERT INTO penyakit (id,nama) VALUES (1,'SARS');
INSERT INTO penyakit (id,nama) VALUES (2,'MERS');
INSERT INTO penyakit (id,nama) VALUES (3,'COVID-19');

CREATE TABLE rulecf ( 
    id INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,
    id_penyakit  INTEGER NOT NULL,
    id_gejala    INTEGER  NOT NULL,
    cfpakar     REAL,
    cfuser      REAL
);

INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (1,1,1,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (2,1,2,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (3,1,5,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (4,1,6,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (5,1,7,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (6,1,4,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (7,1,8,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (8,1,13,0.6,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (9,1,16,0.4,0);

INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (10,2,1,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (11,2,2,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (12,2,8,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (13,2,9,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (14,2,4,0.6,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (15,2,17,0.4,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (16,2,12,0.6,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (17,2,13,0.6,0);

INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (18,3,1,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (19,3,2,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (20,3,3,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (21,3,4,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (22,3,10,0.6,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (23,3,7,0.6,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (24,3,9,0.6,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (25,3,11,0.6,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (26,3,12,0.6,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (27,3,13,0.6,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (28,3,8,0.4,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (29,3,14,0.8,0);
INSERT INTO rulecf (id,id_penyakit,id_gejala,cfpakar,cfuser) VALUES (30,3,15,0.6,0);