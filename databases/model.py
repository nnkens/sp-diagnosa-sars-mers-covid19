import sqlite3
from sqlite3 import Error
from os.path import isfile
#from typing import Tuple


class Sql:
    def __init__(self, table_name):
        self.table_name = table_name

    def init_db(self):
        conn = None
        try:
            conn = sqlite3.connect("databases/sistem_pakar.db")
            with open('databases/schema.sql') as f:
                conn.executescript(f.read())
        except Error as e:
            print(e)
        finally:
            if conn:
                conn.close()

    def add_new_data(self, data):
        if not isfile('databases/sistem_pakar.db'):
            self.init_db()
        conn = None
        result = ''
        try:
            kolom = ' ('
            dtvalues = ' ('
            i = 1
            for x, y in data.items():
                if(i != len(data)):
                    kolom += x + ','
                    dtvalues += '\''+str(y) + '\','
                else:
                    kolom += x
                    dtvalues += '\''+str(y) + '\''
                i = i+1
            kolom += ')'
            dtvalues += ')'
            q = "INSERT INTO "+self.table_name+kolom+" VALUES "+dtvalues
            conn = sqlite3.connect("databases/sistem_pakar.db")
            conn.execute(q,)
            print('Data '+self.table_name+' telah tersimpan.')
            conn.commit()
            result = 'Data '+self.table_name+' telah tersimpan'
        except Error as e:
            print(e)
            result = e
        finally:
            if conn:
                conn.close()
            return (result)

    def update_data(self, data, key, keyval):
        if not isfile('databases/sistem_pakar.db'):
            self.init_db()
        conn = None
        result = ''
        try:
            dtvalues = ''
            i = 1
            for x, y in data.items():
                if(i != len(data)):
                    dtvalues += x+' = '+'\''+str(y) + '\','
                else:
                    dtvalues += x+' = ''\''+str(y) + '\''
                i = i+1
            dtvalues += ''
            q = "UPDATE "+self.table_name+" SET "+dtvalues+" WHERE "+key+" = '"+keyval+"'"
            #print("query:", q)
            conn = sqlite3.connect("databases/sistem_pakar.db")
            conn.execute(q,)
            print('Data '+self.table_name+' telah diupdate.')
            conn.commit()
            result = 'Data '+self.table_name+' telah diupdate'
        except Error as e:
            print(e)
            result = e
        finally:
            if conn:
                conn.close()
            return (result)

    def get_pengetahuan(self):
        result = []
        if not isfile('databases/sistem_pakar.db'):
            self.init_db()
        conn = None
        try:
            conn = sqlite3.connect("databases/sistem_pakar.db")
            result = conn.execute(
                "SELECT c.nama penyakit,b.nama gejala,a.cfpakar,cfuser from rulecf a,gejala b,penyakit c where a.id_gejala=b.id and a.id_penyakit=c.id").fetchall()
        except Error as e:
            print(e)
        finally:
            if conn:
                conn.close()
            return (result)

    def get_data(self):
        result = []
        if not isfile('databases/sistem_pakar.db'):
            self.init_db()
        conn = None
        try:
            conn = sqlite3.connect("databases/sistem_pakar.db")
            result = conn.execute("SELECT * from "+self.table_name).fetchall()
        except Error as e:
            print(e)
        finally:
            if conn:
                conn.close()
            return (result)

    def cari_data(self, field, term):
        result = []
        if not isfile('databases/sistem_pakar.db'):
            self.init_db()
        conn = None
        try:
            conn = sqlite3.connect("databases/sistem_pakar.db")
            my_data = ('%'+term+'%',)
            q = "SELECT * FROM  "+self.table_name+"  WHERE "+field+" like ?"
            result = conn.execute(q, my_data).fetchall()

        except Error as e:
            print(e)
        finally:
            if conn:
                conn.close()
            return (result)

    def del_data(self, key, val):
        conn = None
        result = ''
        try:
            conn = sqlite3.connect('databases/sistem_pakar.db')
            c = conn.cursor()
            c.execute("DELETE FROM "+self.table_name +
                      " WHERE "+key+"='"+val+"'",)
            print('We have deleted id ', val,
                  ' from the table .', self.table_name)
            conn.commit()
            conn.close()
            result = 'We have deleted id ' + \
                str(val)+' from the table .'+self.table_name
        except Error as e:
            print(e)
            result = e
        finally:
            if conn:
                conn.close()
            return (result)

    def del_all(self):
        conn = sqlite3.connect('databases/sistem_pakar.db')
        c = conn.cursor()
        c.execute('DELETE FROM '+self.table_name+';',)
        print('We have deleted', c.rowcount, 'records from the table history.')
        conn.commit()
        conn.close()

    def cek_user(self, uname, pwd):
        result = 0
        if not isfile('databases/sistem_pakar.db'):
            self.init_db()
        conn = None
        try:
            conn = sqlite3.connect("databases/sistem_pakar.db")
            q = "SELECT *  from users WHERE uname = '" + uname+"' AND pwd = '"+pwd+"'"
            result = len(conn.execute(q,).fetchone())
        except Error as e:
            print(e)
        finally:
            if conn:
                conn.close()
            return (result)
