# sp-diagnosa-sars-mers-covid19

**SISTEM PAKAR UNTUK DIAGNOSA PENYAKIT SARS, MERS, DAN COVID-19 MENGGUNAKAN METODE CERTAINTY FACTOR PROSIDING SEMINAR NASIONAL RISET TEKNOLOGI TERAPAN: 2020.**

> SARS, MERS, dan COVID-19 merupakan penyakit infeksi virus pada saluran pernapasan yang bisa berakibat fatal. Ketiga penyakit tersebut disebabkan oleh virus yang berasal dari keluarga yang sama. Ketiganya sama-sama disebabkan oleh virus korona. SARS disebabkan oleh SARS-CoV, MERS disebabkan oleh MERS-CoV, dan virus korona Wuhan disebabkan oleh 2019-nCoV (berganti nama menjadi COVID-19). Makalah ini bertujuan untuk mengimplementasikan Sistem Pakar metode Certainty Factor guna mendiagnosa gejala ketiga penyakit. Metode ini dapat mengatasi ketidakpastian dalam pengambilan keputusan berdasarkan gejala-gejala yang dirasakan atau diinputkan oleh user. Metode ini cocok dipakai dalam sistem pakar untuk mengukur sesuatu apakah pasti atau tidak pasti dalam mendiagnosis penyakit sebagai salah satu contohnya. Berdasarkan hasil perhitungan, sistem pakar dapat digunakan untuk diagnosa awal terhadap ketiga penyakit tersebut.


> Metode sistem pakar yang digunakan adalah metode Certainty Factor (CF). Teori Certainty Factor (CF) adalah untuk mengakomodasi ketidakpastian pemikiran (inexact reasoning) seorang pakar yang diusulkan oleh Shortliffe dan Buchanan pada tahun 1975. Seorang pakar (misalnya dokter) sering menganalisis informasi yang dengan ungka pan dengan ketidakpastian, untuk mengakomodasikan hal ini digunakan CF guna menggambarkan tingkat keyakinan pakar terhadap masalah yang sedang dihadapi. Certainty Factor (Faktor Ketidak pastian) menyatakan kepercayaan dalam sebuah kejadian (fakta atau hipotesa) berdasarkan bukti atau penilaian pakar. Metode CF menggunakan suatu nilai untuk mengansumsikan derajat keyakinan seorang pakar terhadap suatu data. Metode CF memperkenalkan konsep keyakinan dan ketidakyakinan. Berikut ini adalah rumus metode CF untuk mengansumsikan kepastian seorang pakar terhadap suatu data.

```
CF[H,E] = CF[H] * CF[E] .... (1)
CFcombine CF[H,E]1,2 = CF[H,E]1 + CF[H,E]2 *(1CF[H,E]1)..........(2) 
CFcombineCF[H,E]old3 = CF[H,E]old + CF[H,E]3* (1CF[H,E]old)......(3)
CF = Certainty Factor (Faktor Kepastian) dalam hipotesa H yang dipengaruhi oleh Fakta E.
E = Evidence (Peristiwa atau Fakta)
```



